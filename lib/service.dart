import 'dart:convert';
import 'package:api_recipe_search/globals.dart';
import 'package:http/http.dart' as http;

import 'package:api_recipe_search/recipe.dart';

class Service {

  Future<Recipe> fetchActivity() async {
    final response = await http.get('${Globals.API_LINK}?q=${Globals.parameters['q']}&app_id=${Globals.parameters['app_id']}&app_key=${Globals.parameters['app_key']}&from=${Globals.page * 20}&to=${Globals.page * 20 + 20}');

    if (response.statusCode == 200) {
      Recipe.fromJsonAll(jsonDecode(response.body));
      return Recipe.fromJson(jsonDecode(response.body));
    }
    else {
      throw Exception('Failed to load the recipe list');
    }
  }

}