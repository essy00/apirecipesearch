import 'package:api_recipe_search/pages/details.dart';
import 'package:api_recipe_search/pages/saved_recipes.dart';
import 'package:flutter/material.dart';
import 'package:api_recipe_search/pages/results.dart';
import 'package:api_recipe_search/pages/home.dart';
import 'package:api_recipe_search/pages/recipe_search.dart';

void main() => runApp(MaterialApp(
  initialRoute: Home.routeName,
  routes: {
    Home.routeName: (context) => Home(),
    SavedRecipes.routeName: (context) => SavedRecipes(),
    RecipeSearch.routeName: (context) => RecipeSearch(),
    Results.routeName: (context) => Results(),
    DetailsPage.routeName: (context) => DetailsPage(),
  },
));