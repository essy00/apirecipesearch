import 'package:api_recipe_search/recipe.dart';

class Globals {
  static const String API_LINK = 'https://api.edamam.com/search';
  static const String ID_VALUE = 'd7ce0dc3';
  static const String TOKEN_VALUE = '31f48028d039eacf51a276cc5c0b4093';
  static var parameters = {
    'id': 0,
    'q': '',
    'app_id': ID_VALUE,
    'app_key': TOKEN_VALUE,
  };

  static List recipes = [];
  static Recipe recipe;
  static bool newSearch = true;
  static int page = 0;

  static List savedRecipes = [];




  // Languages
  static Map englishLanguage = {
    'home': 'Home',
    'titleMainPage': 'Recipe Search/Save',
    'buttonRecipeSearch': 'Recipe Search',
    'buttonSavedRecipes': 'Saved Recipes',

    'appBarRecipeSearch': 'Recipe Search',
    'informationField': 'Enter a key term',
    'errorField': 'Key term must be greater than 2 characters',
    'minCalField': 'Minimum Calorie',
    'maxCalField': 'Maximum Calorie',
    'errorNumber': 'Calorie can not be smaller than 0',
    'errorCal': 'Maximum calorie can not be smaller than minimum calorie',
    'submitButton': 'Submit',

    'searchPage': 'Search Page',

    'calories': 'Calories',
    'ingredients': 'Ingredients',
    'steps': 'Steps',
    'category': 'Category:',
    'noImages': 'No Images',
    'saveRecipe': 'Save the Recipe',
    'saved': 'Saved',
    'remove': 'Remove the Recipe',

    'appBarSavedRecipes': 'Saved Recipes',
  };
  static Map spanishLanguage = {
    'home': 'Inicio',
    'titleMainPage': 'Buscar/Guardar Recetas',
    'buttonRecipeSearch': 'Buscar Recetas',
    'buttonSavedRecipes': 'Recetas Guardadas',

    'appBarRecipeSearch': 'Buscar Recetas',
    'informationField': 'Entrar un término clave',
    'errorField': 'El término clave debe de tener más de 2 letras',
    'minCalField': 'Caloría mínima',
    'maxCalField': 'Caloría máxima',
    'errorNumber': 'Caloría no puede ser menor que 0',
    'errorCal': 'Caloría máxima no puede ser menor que caloría mínima',
    'submitButton': 'Buscar',

    'searchPage': 'Página de Búsqueda',

    'calories': 'Calorías',
    'ingredients': 'Ingredientes',
    'steps': 'Pasos',
    'category': 'Categoría:',
    'noImages': 'Sin imágenes',
    'saveRecipe': 'Guardar la Receta',
    'saved': 'Guardada',
    'remove': 'Eliminar la Receta',

    'appBarSavedRecipes': 'Recetas Guardadas',
  };

  static Map language = englishLanguage;
}