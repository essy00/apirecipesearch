import 'package:api_recipe_search/globals.dart';
import 'package:flutter/cupertino.dart';

class Recipe {
  int id;
  String label;
  String image;
  String source;
  double yield;
  List dietLabels;
  List healthLabels;
  List ingredientLines;
  List ingredients;
  double calories;

  bool saved = false;

  Recipe({this.id = 0, @required this.label, @required this.image,
    @required this.source, @required this.yield,
    @required this.dietLabels, @required this.healthLabels,
    @required this.ingredientLines, @required this.ingredients,
    @required this.calories
  });

  factory Recipe.fromJson(Map<String, dynamic> json) {
    return Recipe(
      label: json['hits'][Globals.parameters['id']]['recipe']['label'],
      image: json['hits'][Globals.parameters['id']]['recipe']['image'],
      source: json['hits'][Globals.parameters['id']]['recipe']['source'],
      yield: json['hits'][Globals.parameters['id']]['recipe']['yield'],
      dietLabels: json['hits'][Globals.parameters['id']]['recipe']['dietLabels'],
      healthLabels: json['hits'][Globals.parameters['id']]['recipe']['healthLabels'],
      ingredientLines: json['hits'][Globals.parameters['id']]['recipe']['ingredientLines'],
      ingredients: json['hits'][Globals.parameters['id']]['recipe']['ingredients'],
      calories: json['hits'][Globals.parameters['id']]['recipe']['calories'],
    );
  }

  Recipe.fromJsonAll(Map<String, dynamic> json) {
    if(Globals.newSearch == true) {
      Globals.recipes = [];
    }
    for(var recipe in json['hits']) {
      Globals.recipes.add(Recipe(
        label: recipe['recipe']['label'],
        image: recipe['recipe']['image'],
        source: recipe['recipe']['source'],
        yield: recipe['recipe']['yield'],
        dietLabels: recipe['recipe']['dietLabels'],
        healthLabels: recipe['recipe']['healthLabels'],
        ingredientLines: recipe['recipe']['ingredientLines'],
        ingredients: recipe['recipe']['ingredients'],
        calories: recipe['recipe']['calories'],
      ));
    }
  }
}