import 'package:api_recipe_search/globals.dart';
import 'package:api_recipe_search/pages/recipe_search.dart';
import 'package:api_recipe_search/pages/saved_recipes.dart';
import 'package:flutter/material.dart';

import 'package:flutter_speed_dial/flutter_speed_dial.dart';

class Home extends StatefulWidget {

  static const routeName = '/home';

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[800],
      appBar: AppBar(
        backgroundColor: Colors.grey[700],
        title: Text(Globals.language['home']),
        centerTitle: true,
      ),
      floatingActionButton: SpeedDial(
        icon: Icons.language,
        backgroundColor: Colors.white,
        overlayColor: Colors.grey[400],
        buttonSize: 70,
        children: [
          SpeedDialChild(
            child: Icon(Icons.language),
            label: 'español',
            labelStyle: TextStyle(
              fontWeight: FontWeight.bold,
            ),
            onTap: () => setState(() { Globals.language = Globals.spanishLanguage; }),
          ),
          SpeedDialChild(
              child: Icon(Icons.language),
              label: 'English',
              labelStyle: TextStyle(
                  fontWeight: FontWeight.bold,
              ),
              onTap: () => setState(() { Globals.language = Globals.englishLanguage; }),
          ),
        ],
      ),
      body: Center(
        child: Column(
          children: [
            SizedBox(height: 25,),
            Text(
              Globals.language['titleMainPage'],
              style: TextStyle(
                fontSize: 28,
                color: Colors.white,
                fontFamily: 'Raleway',
              ),
            ),
            SizedBox(height: 250,),
            ElevatedButton.icon(
                onPressed: () {
                  Navigator.pushNamed(context, RecipeSearch.routeName);
                },
                icon: Icon(Icons.search),
                label: Text(Globals.language['buttonRecipeSearch']),
            ),
            ElevatedButton.icon(
                onPressed: () {
                  Navigator.pushNamed(context, SavedRecipes.routeName);
                },
                icon: Icon(Icons.save),
                label: Text(Globals.language['buttonSavedRecipes']),
              style: ButtonStyle(
                backgroundColor: MaterialStateProperty.all<Color>(Colors.green),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
