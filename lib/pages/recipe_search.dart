import 'package:api_recipe_search/globals.dart';
import 'package:flutter/material.dart';
import 'package:api_recipe_search/recipe_form.dart';

class RecipeSearch extends StatelessWidget {

  static const routeName = '/search';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[800],
      appBar: AppBar(
        backgroundColor: Colors.grey[700],
        title: Text(Globals.language['appBarRecipeSearch']),
        centerTitle: true,
      ),
      body: Padding(
        padding: const EdgeInsets.all(12.0),
        child: RecipeForm(),
      ),
    );
  }
}
