import 'package:flutter/material.dart';
import 'package:api_recipe_search/globals.dart';

class DetailsPage extends StatefulWidget {

  static const routeName = '/detail';

  @override
  _DetailsPageState createState() => _DetailsPageState();
}

class _DetailsPageState extends State<DetailsPage> {

  String buttonText;

  @override
  void initState() {
    super.initState();
    if(Globals.recipe.saved) {
      buttonText = Globals.language['remove'];
    }
    else {
      buttonText = Globals.language['saveRecipe'];
    }
  }

  Widget ifImageExists(step) {
    if(step['image'] != null) {
      return Center(
          child: Column(
            children: [
              Image.network(step['image']),
              Text(
                '-----',
                style: TextStyle(
                  fontSize: 17,
                  color: Colors.grey[300],
                ),
              )
            ],
          )
      );
    }
    else {
      return Center(
        child: Column(
          children: [
            Text(
              Globals.language['noImages'],
              style: TextStyle(
                fontSize: 17,
                color: Colors.grey[300],
              ),
            ),
            Text(
              '-----',
              style: TextStyle(
                fontSize: 17,
                color: Colors.grey[300],
              ),
            )
          ],
        )
      );
    }
  }

  Widget validButton() {
    if(Globals.recipe.saved == false){
      return ElevatedButton.icon(
        onPressed: () {
          Globals.savedRecipes.add(Globals.recipe);
          Globals.recipe.saved = true;
          setState(() {});
          buttonText = Globals.language['remove'];
        },
        icon: Icon(Icons.save),
        label: Text(buttonText),
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all<Color>(Colors.green),
        ),
      );
    }
    else {
      return ElevatedButton.icon(
        onPressed: () {
          Globals.savedRecipes.remove(Globals.recipe);
          Globals.recipe.saved = false;
          setState(() {});
          buttonText = Globals.language['saveRecipe'];
        },
        icon: Icon(Icons.delete),
        label: Text(buttonText),
        style: ButtonStyle(
          backgroundColor: MaterialStateProperty.all<Color>(Colors.red),
        ),
      );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[800],
      appBar: AppBar(
        backgroundColor: Colors.grey[700],
        title: Text(Globals.recipe.label),
        centerTitle: true,
      ),
      body: ListView.builder(
        itemCount: 1,
        itemBuilder: (BuildContext context, int index) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.stretch,
            children: [
              Card(
                margin: EdgeInsets.fromLTRB(16, 16, 16, 0),
                color: Colors.grey[600],
                child: Column(
                  children: [
                    Text(
                        Globals.recipe.label,
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                      ),
                    ),
                    SizedBox(height: 5,),
                    Image.network(Globals.recipe.image),
                    SizedBox(height: 5,),
                    Text(
                      '${Globals.recipe.calories.round()} ${Globals.language['calories']}',
                      style: TextStyle(
                        fontSize: 17,
                        fontWeight: FontWeight.bold,
                        color: Colors.black,
                      ),
                    ),
                    SizedBox(height: 10,),
                    Text('${Globals.language['ingredients']}', style: TextStyle(fontSize: 19, fontWeight: FontWeight.bold, color: Colors.black),),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: Globals.recipe.ingredientLines.map((ingredient) => Text(
                          ingredient,
                        style: TextStyle(
                          fontSize: 17,
                          color: Colors.grey[300],
                        ),
                      )).toList(),
                    ),
                    SizedBox(height: 20,),
                    Text('${Globals.language['steps']}', style: TextStyle(fontSize: 19, fontWeight: FontWeight.bold, color: Colors.black),),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: Globals.recipe.ingredients.map((step) => Column(
                        crossAxisAlignment: CrossAxisAlignment.stretch,
                        children: [
                          Text(
                            '${step['text']}',
                            style: TextStyle(
                              fontSize: 17,
                              color: Colors.grey[300],
                            ),
                          ),
                          Text(
                            '${Globals.language['category']} ${step['foodCategory']}',
                            style: TextStyle(
                              fontSize: 17,
                              color: Colors.grey[300],
                            ),
                          ),
                          SizedBox(height: 10,),
                          ifImageExists(step),
                          SizedBox(height: 10,),
                        ],
                      )).toList(),
                    ),
                    validButton(),
                  ],
                ),
              )
            ],
          );
        }
      )
    );
  }
}
