import 'package:flutter/material.dart';
import 'package:api_recipe_search/globals.dart';
import 'package:api_recipe_search/pages/details.dart';

class SavedRecipes extends StatefulWidget {

  static const routeName = '/savedRecipes';

  @override
  _SavedRecipesState createState() => _SavedRecipesState();
}

class _SavedRecipesState extends State<SavedRecipes> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[800],
      appBar: AppBar(
        backgroundColor: Colors.grey[700],
        title: Text(Globals.language['appBarSavedRecipes']),
        centerTitle: true,
      ),
      body: Center(
        child: ListView.builder(
          itemCount: 1,
          itemBuilder: (BuildContext context, int index) {
            return Padding(
                padding: const EdgeInsets.all(10),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: Globals.savedRecipes.reversed.toList().map((recipe) => Card(
                      margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                      color: Colors.grey[600],
                      child: Column(
                        children: [
                          TextButton(
                            onPressed: () {
                              Globals.recipe = recipe;
                              Navigator.pushNamed(context, DetailsPage.routeName) .then((value) => setState(() {}));
                            },
                            child: Text(
                              '${recipe.label}',
                              style: TextStyle(
                                fontSize: 17,
                              ),
                            ),
                            style: TextButton.styleFrom(
                              primary: Colors.white,
                            ),
                          ),
                          Image.network(recipe.image),
                          SizedBox(height: 15,)
                        ],
                      )
                  )).toList(),
                )
            );
          },
        ),
      ),
    );
  }
}
