import 'dart:async';
import 'package:api_recipe_search/globals.dart';
import 'package:api_recipe_search/pages/details.dart';
import 'package:flutter/material.dart';
import 'package:api_recipe_search/recipe.dart';
import 'package:api_recipe_search/service.dart';

class Results extends StatefulWidget {

  static const routeName = '/results';

  @override
  _ResultsState createState() => _ResultsState();
}

class _ResultsState extends State<Results> {

  Future<Recipe> futureRecipe;
  Service apiManager = Service();
  ScrollController _scrollController = new ScrollController();

  @override
  void initState() {
    super.initState();
    Globals.page = 0;
    Globals.newSearch = true;
    futureRecipe = apiManager.fetchActivity();

    _scrollController.addListener(() {
      if(_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
        if(Globals.page < 4){
          Globals.newSearch = false;
          Globals.page++;
          futureRecipe = apiManager.fetchActivity();
          setState(() {});
        }
      }
    });

    print('Minimum Calorie: ${Globals.parameters['minCal']}\nMaximum Calorie: ${Globals.parameters['maxCal']}');
  }

  @override
  void dispose() {
    super.dispose();
    _scrollController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.grey[800],
      appBar: AppBar(
        backgroundColor: Colors.grey[700],
        title: Text(Globals.language['searchPage']),
        centerTitle: true,
      ),
      body: Center(
          child: _futureRecipe(context)
      ),
    );
  }

  _futureRecipe(context) {
    return FutureBuilder<Recipe>(
      future: futureRecipe,
      builder: (context, snapshot) {
        if(snapshot.hasData) {
          return ListView.builder(
            controller: _scrollController,
            itemCount: 1,
            itemBuilder: (BuildContext context, int index) {
              return Padding(
                  padding: const EdgeInsets.all(10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: Globals.recipes.map((recipe) => Card(
                        margin: EdgeInsets.fromLTRB(10, 10, 10, 0),
                        color: Colors.grey[600],
                        child: Column(
                          children: [
                            TextButton(
                              onPressed: () {
                                Globals.recipe = recipe;
                                Navigator.pushNamed(context, DetailsPage.routeName);
                              },
                              child: Text(
                                '${recipe.label}',
                                style: TextStyle(
                                  fontSize: 17,
                                ),
                              ),
                              style: TextButton.styleFrom(
                                primary: Colors.white,
                              ),
                            ),
                            Image.network(recipe.image),
                            SizedBox(height: 15,)
                          ],
                        )
                    )).toList(),
                  )
              );
            },
          );
        }
        else if (snapshot.hasError) {
          return Text('${snapshot.error}');
        }

        return Center(child: CircularProgressIndicator());
      },
    );
  }
}
