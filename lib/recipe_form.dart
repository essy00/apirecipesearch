import 'package:api_recipe_search/globals.dart';
import 'package:api_recipe_search/pages/results.dart';
import 'package:flutter/material.dart';

// Define a custom Form widget.
class RecipeForm extends StatefulWidget {
  @override
  RecipeFormState createState() {
    return RecipeFormState();
  }
}

class RecipeFormState extends State<RecipeForm> {
  final _formKey = GlobalKey<FormState>();
  bool filledSearchField;

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Column(
        children: [
          TextFormField(
            validator: (value){
              if(value.length < 2) {
                filledSearchField = false;
                return Globals.language['errorField'];
              } else {
                filledSearchField = true;
              }
            },
            onSaved: (String val) => setState(() => Globals.parameters['q'] = val),
            decoration: InputDecoration(
              helperText: Globals.language['informationField'],
              helperStyle: TextStyle(
                color: Colors.white,
              ),
            ),
            style: TextStyle(
              color: Colors.white,
            ),
          ),
          ElevatedButton(
            onPressed: () {
              _formKey.currentState.save();
              _formKey.currentState.validate();
              if(filledSearchField) {
                Navigator.pushNamed(context, Results.routeName) .then((value) => setState(() {}));
              } else {

              }
            },
            child: Text(Globals.language['submitButton']),
          )
        ],
      ),
    );
  }
}